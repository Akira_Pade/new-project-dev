<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Testaccount</fullName>
        <description>Testaccount</description>
        <protected>false</protected>
        <recipients>
            <recipient>akira@sfdc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>suresh@dx.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Skonda</template>
    </alerts>
    <alerts>
        <fullName>Testaccount1</fullName>
        <description>Testaccount</description>
        <protected>false</protected>
        <recipients>
            <recipient>akira@sfdc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>suresh@dx.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Skonda</template>
    </alerts>
    <alerts>
        <fullName>Testaccount2</fullName>
        <ccEmails>m.koti2143@gmail.com</ccEmails>
        <description>Testaccount2</description>
        <protected>false</protected>
        <recipients>
            <recipient>suresh@dx.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Skonda</template>
    </alerts>
    <fieldUpdates>
        <fullName>testfield</fullName>
        <field>Industry</field>
        <literalValue>Banking</literalValue>
        <name>testfield</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Skonda</fullName>
        <active>true</active>
        <formula>CreatedDate  =  LastModifiedDate</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Testaccount2</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>read</fullName>
        <actions>
            <name>Testaccount1</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.AnnualRevenue</field>
            <operation>greaterThan</operation>
            <value>500</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>test</fullName>
        <actions>
            <name>Testaccount</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>testfield</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>suresh</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
